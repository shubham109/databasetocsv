const express = require('express')
const oracledb = require('oracledb')
const db = require('./db')
const Json2csvParser = require("json2csv").Parser;
const fs = require("fs");
const app = express()
let connection = ''
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;    //this will help to save column name of database in csv
oracledb.autoCommit = true;
app.get('/show',async(req,res)=>{
    try{
        connection = await oracledb.getConnection(db);
        const result = await connection.execute(
                        `SELECT * FROM employee_master`
                    );
                    res.send(result.rows)
                    //console.log(result.rows)
                    const jsonData = JSON.parse(JSON.stringify(result.rows));
                    //console.log("jsonData", jsonData);

                     const json2csvParser = new Json2csvParser({ header: true});
                     const csv = json2csvParser.parse(jsonData);
                     console.log(csv)
                    fs.writeFile("data.csv", csv, function(error) {
                    if (error) throw error;
                    console.log("Write to Data.csv successfully!"); 
                });
    }
    catch(err){
        console.log(err)
        return res.send("Error")
    }
})

app.listen(3000,(err,res)=>{ 
    if (err) throw err
})
